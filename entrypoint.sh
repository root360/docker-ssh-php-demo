#!/bin/bash

ssh-keygen -A

echo "$AUTHORIZED_KEY" > /home/demo/.ssh/authorized_keys

/usr/sbin/sshd -D -e
