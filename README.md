# docker-ssh-php-demo

## Start

```
docker run --rm -it -p 2222:22 -e "AUTHORIZED_KEY=ssh-rsa AAAA..." registry.gitlab.com/root360/docker-ssh-php-demo
```

## Connect


```
ssh demo@localhost -p 2222
```

## Test

```
$ php index.php 
Hello World
```
