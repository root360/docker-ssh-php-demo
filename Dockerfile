FROM alpine:latest

RUN apk --no-cache add bash openssh php7 \
    && addgroup demo \
    && adduser -G demo -s /bin/bash -D demo \
    && passwd -u demo \
    && install -d -m 700 -o demo -g demo /home/demo/.ssh \
    && install -m 600 -o demo -g demo /dev/null /home/demo/.ssh/authorized_keys


EXPOSE 22
 
COPY --chown=demo:demo index.php /home/demo/index.php

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
